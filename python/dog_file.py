# Class
# attributes: variables
# methods: functions

a_very_important_piece_of_info = 230498234

class Dog:
    # __init__ : this is called the CONSTRUCTOR
    def __init__(self, name, age, eye_color):
        self.name = name
        self.age = age
        self.eye_color = eye_color

    
    def speak(self):
        print(f"{self.name} says: 'woof!'")
    
    def info(self):
        print(f'About this {self.name}:')
        print(f'- Name: \t{self.name}')
        print(f'- age: \t\t{self.age}')
        print(f'- eye_color: \t{self.eye_color}')
        print('='*20)


###############
# This is where my script will really start
###############

# nike = Dog('Nike', 10, 'brown')
# nike.info()

# white_fang = Dog('White Fang', 12, 'grey')
# white_fang.info()


