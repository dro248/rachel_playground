from dog_file import Dog, a_very_important_piece_of_info
import json

nike = Dog('Nike', 10, 'brown')
nike.info()
print(a_very_important_piece_of_info)

# This is called a "Context Manager" -- useful for reading/writing files; it auto closes files when you are done :)
with open('my_info_stuff.json', 'r') as json_file:
    # Read the data from 'my_info_stuff.json' (which we refer to as `json_file`) as a dictionary
    my_info = json.load(json_file)

# my_info now should contain a dictionary (provided that `json_file` is well-formated JSON)
my_info
