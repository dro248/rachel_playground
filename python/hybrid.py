class Car:
    # These things are important
    # ...


class Hybrid(Car):
    # ...


class ConventionalCar(Car):
    # ...
