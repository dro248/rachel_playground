quit_text = 'quit'

while True:
    x = input('Please write something: ')

    if x == quit_text:
        print('Quitting! Goodbye!')
        break

    print(f'You typed: {x}')
